var directions = ["northwest", "north", "northeast", "west", "east", "southwest", "south", "southeast"]

function coordTravel(start, direction, amount) {
	coords = toCoords(start)
	amount = Number(amount)
	if ( direction === "northwest" ) {		
		coords[0] = coords[0]-amount
		coords[1] = coords[1]-amount
	}else if ( direction === "north" ) {
		coords[1] = coords[1]-amount
	}else if ( direction === "northeast" ) {
		coords[0] = coords[0]+amount
		coords[1] = coords[1]-amount
	}else if ( direction === "west" ) {
		coords[0] = coords[0]-amount
	}else if ( direction === "east" ) {
		coords[0] = coords[0]+amount
	}else if ( direction === "southwest" ) {
		coords[0] = coords[0]-amount
		coords[1] = coords[1]+amount
	}else if ( direction === "south" ) {
		coords[1] = coords[1]+amount
	}else if ( direction === "southeast" ) {
		coords[0] = coords[0]+amount
		coords[1] = coords[1]+amount
	}else{
		console.log("coordianteTravel: Invalid direction, returning null.")
		return
	}
	if ( coords[0] < 0 || coords[1] < 0 ) {
		console.log("coordianteTravel: a coordinate is negative, returning null.")
		return
	}
	return toBox(coords[0].toString(), coords[1].toString())
}

function getOppositeDirection(inputDirection) {
	if ( inputDirection === "northwest" ) {
		return "southeast"
	}else if ( inputDirection === "north" ) {
		return "south"
	}else if ( inputDirection === "northeast" ) {
		return "southwest"
	}else if ( inputDirection === "west" ) {
		return "east"
	}else if ( inputDirection === "east" ) {
		return "west"
	}else if ( inputDirection === "southwest" ) {
		return "northeast"
	}else if ( inputDirection === "south" ) {
		return "north"
	}else if ( inputDirection === "southeast" ) {
		return "northwest"
	}else{
		console.log("getOppositeDirection: You broke something, returning null.")
		return
	}
}

function cascade(x, y) {
	x = Number(x)
	y = Number(y)
	surrounding = []
	surrounding.push( "("+	(x-1)	+","+	(y-1)	+")" )
	surrounding.push( "("+	(x)		+","+	(y-1)	+")" )
	surrounding.push( "("+	(x+1)	+","+	(y-1)	+")" )
	surrounding.push( "("+	(x-1)	+","+	(y)		+")" )
	surrounding.push( "("+	(x+1)	+","+	(y)		+")" )
	surrounding.push( "("+	(x-1)	+","+	(y+1)	+")" )
	surrounding.push( "("+	(x)		+","+	(y+1)	+")" )
	surrounding.push( "("+	(x+1)	+","+	(y+1)	+")" )
	return surrounding
}
