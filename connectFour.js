var player1 = ["red"]
var player2 = ["blue"]
var gameOver = false
var turn = player1
var boxesDrawn = false
document.addEventListener("DOMContentLoaded", drawBoxes())

function drawBoxes() {
	console.log("drawBoxes() has been initialized.")
	for (y = 1; y <= 6; y++) {
		for (x = 1; x <= 7; x++) { 
			self = document.getElementById("centerdiv").innerHTML
			document.getElementById("centerdiv").innerHTML = self + "<div id=("+x+","+y+") onclick=\"updateGame('"+x+"', '"+y+"')\"></div>"
			document.getElementById("("+x+","+y+")").style.backgroundColor = "grey"
			
		}
	}
}

function toCoords(box) {
	return [Number(box[1]), Number(box[3])]
}

function toBox(x, y) {
	toReturn = "("+x.toString()+","+y.toString()+")"
	return toReturn
}

function winDetect(x, y) {
	startBox = toBox(x, y)
	colourToDetect = document.getElementById("("+x+","+y+")").style.backgroundColor
	surrounding = cascade(x, y)
	winChecked = false
	adjacents = false
	adjacentToOriginal = []
	possibleDirections = []
	for (box = 0; box < surrounding.length; box++) {
		if ( document.getElementById(surrounding[box] ) !== null ) {
			if ( document.getElementById(surrounding[box]).style.backgroundColor === colourToDetect ) {
				console.log("Adjacency Found!")
				adjacentToOriginal.push(surrounding[box])
				possibleDirections.push(directions[box])
				adjacents = true
			}
		}
	}
	if ( adjacents === true ){
		console.log("Adjacents: "+adjacentToOriginal)
		console.log("Directions: "+possibleDirections)
	}
	console.log("-----")
	for (adjacent = 0; adjacent < adjacentToOriginal.length; adjacent++) {
		numberInARow = 1
		endOfActual = false
		endOfOpposite = false
		check = 1
		while ( endOfActual === false ) {
			checkBox = coordTravel(adjacentToOriginal[adjacent], possibleDirections[adjacent], check)
			if ( document.getElementById(checkBox) !== null && document.getElementById(checkBox).style.backgroundColor !== colourToDetect ) {
				endOfActual = true
			}else if ( document.getElementById(checkBox) === null ) {
				endOfActual = true
			}else{
				check = check + 1
				numberInARow = numberInARow + 1
			}
		}
		check = 1
		while ( endOfOpposite === false ) {
			checkBox = coordTravel(adjacentToOriginal[adjacent], getOppositeDirection(possibleDirections[adjacent]), check)
			if ( document.getElementById(checkBox) !== null && document.getElementById(checkBox).style.backgroundColor !== colourToDetect ) {
				endOfOpposite = true
			}else if ( document.getElementById(checkBox) === null ) {
				endOfOpposite = true
			}else{
				check = check + 1
				numberInARow = numberInARow + 1
			}
		}
		if ( numberInARow >= 4 ) {
			console.log("Win detected.")
			gameOver = true
		}
	}
}

function resetGame() {
	document.getElementById("centerdiv").innerHTML = "<h1>Connect Four</h1>"
	boxesDrawn = false
	drawBoxes()
	gameOver = false
	turn = player1
	document.getElementById("turn").innerHTML = "<h1>Player 1's Turn [Red]</h1>"
}

function updateGame(x, y) {
	console.log("update(box) has been initialized. Box: ("+x+","+y+").")
	doneUpdate = false
	loop = 0
	y = 6
	if ( gameOver === false ) {
		while (doneUpdate === false) {
			loop = loop + 1
			if ( loop > 25 ) {
				break
			}
			console.log("("+x+","+y+")")
			obj = document.getElementById("("+x+","+y+")");
			if ( obj.style.backgroundColor !== "red" && obj.style.backgroundColor !== "blue" ) {
				obj.style.backgroundColor = turn
				winDetect(x, y)
				if ( gameOver === false ) {
					if (turn === player1) {
						document.getElementById("turn").innerHTML = "<h1>Player 2's Turn [Blue]</h1>"
						turn = player2
					}else{
						document.getElementById("turn").innerHTML = "<h1>Player 1's Turn [Red]</h1>"
						turn = player1
					}
				}
				doneUpdate = true	
			}else{
				y = y - 1
				if (y === 0) {
					if (turn === player1) {
						document.getElementById("turn").innerHTML = "<h1>Player 1's Turn [Red] - Column Full!</h1>"
					}else{
						document.getElementById("turn").innerHTML = "<h1>Player 2's Turn [Blue] - Column Full!</h1>"
					}
					doneUpdate = true
				}
			}
		}
	}
	if ( gameOver === true ) {
		if (turn === player1) {
			document.getElementById("turn").innerHTML = '<h1>Player 1 [Red] Has Won The Game!          <button type="button" onclick="resetGame()">Click To Reset</button></h1>'
		}else{
			document.getElementById("turn").innerHTML = '<h1>Player 2 [Blue] Has Won The Game!          <button type="button" onclick="resetGame()">Click To Reset</button></h1>'
		}
	}
}
